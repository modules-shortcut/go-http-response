package main

import (
	"gitlab.com/modules-shortcut/go-http-response/responses"
	"gitlab.com/modules-shortcut/go-utils"
)

type data struct {
	ID      uint   `json:"id"`
	Name    string `json:"name"`
	Age     uint   `json:"age"`
	Friends []data `json:"friends"`
}

func main() {
	var datas = []data{
		{
			ID:   1,
			Name: "dayat",
			Age:  26,
			Friends: []data{
				{
					ID:   4,
					Name: "otong",
					Age:  26,
					Friends: []data{
						{
							ID:   7,
							Name: "ryan",
							Age:  26,
						},
						{
							ID:   8,
							Name: "ali",
							Age:  25,
						},
					},
				},
				{
					ID:   5,
					Name: "bambang",
					Age:  26,
				},
				{
					ID:   6,
					Name: "fiktor",
					Age:  25,
				},
			},
		},
		{
			ID:   2,
			Name: "celi",
			Age:  26,
		},
		{
			ID:   3,
			Name: "mail",
			Age:  25,
		},
	}

	var serialize = responses.NewSerialize(datas, "friends.friends.name", "friends.friends.friends", "friends.friends.id", "id")

	utils.PrettyPrint(serialize.Exclude())
}
