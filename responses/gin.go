package responses

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/modules-shortcut/go-utils"
	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
	"net/http"
	"strings"
)

var (
	excludeResponseHeaders string
	headers                []string
	statusCode             bool
)

type Data struct {
	Result     interface{} `json:"result"`
	Pagination interface{} `json:"pagination,omitempty"`
}

type Response struct {
	Code    string      `json:"code,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Extra   interface{} `json:"extra,omitempty"`
	IsError bool        `json:"is_error"`
	Message string      `json:"message"`
	Status  int         `json:"status"`
}

func (res Response) Byte() (b []byte) {
	b, _ = json.Marshal(res)
	return
}

func (res Response) String() string {
	return string(res.Byte())
}

//M for message
type M struct {
	//C for code
	C string
	//M for message
	M string
	//E for errors
	E error
}

func init() {
	excludeResponseHeaders = utils.GetEnvString("EXCLUDED_HEADERS", "")
	excludeResponseHeaders = strings.ReplaceAll(excludeResponseHeaders, " ", "")
	headers = strings.Split(excludeResponseHeaders, ",")
	statusCode = utils.GetEnvBool("STATUS_CODE", false)
}

func loopArgs(ctx *gin.Context, statusCode int, msg M, args []interface{}) (res Response) {

	//Init value
	res.Code = msg.C
	res.IsError = false
	res.Message = msg.M
	res.Status = statusCode

	if msg.E != nil && msg.M == "" {
		res.Message = msg.E.Error()
	}

	//if success
	ctx.Writer.Header().Set("X-SUCCESS-CODE", res.Code)
	ctx.Writer.Header().Set("X-SUCCESS-MESSAGE", res.Message)

	//if error
	if statusCode > 201 {
		ctx.Writer.Header().Set("X-ERROR-CODE", res.Code)
		ctx.Writer.Header().Set("X-ERROR-MESSAGE", res.Message)
		res.IsError = true
	}

	for idx, elm := range args {
		if idx == 0 {
			res.Data = elm
		}
		if idx > 0 {
			if len(args[1:]) > 1 {
				res.Extra = args[1:]
			} else {
				res.Extra = elm
			}
		}
	}

	//Excluded headers
	for _, elm := range headers {
		ctx.Writer.Header().Del(elm)
	}

	return
}

func NotFoundOrBadRequest(ctx *gin.Context, msg M, args ...interface{}) {
	if errors.Is(msg.E, mongo.ErrNoDocuments) {
		NotFound(ctx, msg, args...)
		return
	}
	if errors.Is(msg.E, gorm.ErrRecordNotFound) {
		NotFound(ctx, msg, args...)
		return
	}

	BadRequest(ctx, msg, args...)
	return
}

// Ok have 3 args (message,data,extra)
func Ok(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(http.StatusOK, loopArgs(ctx, http.StatusOK, msg, args))
	return
}

// Created have 3 args (message,data,extra)
func Created(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusCreated, http.StatusOK).(int), loopArgs(ctx, http.StatusCreated, msg, args))
	return
}

// NoContent have 3 args (message,data,extra)
func NoContent(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.Status(http.StatusNoContent)
	return
}

// Client error responses (4xx)

// BadRequest have 3 args (message,data,extra)
func BadRequest(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusBadRequest, http.StatusOK).(int), loopArgs(ctx, http.StatusBadRequest, msg, args))
	return
}

// Unauthorized have 3 args (message,data,extra)
func Unauthorized(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusUnauthorized, http.StatusOK).(int), loopArgs(ctx, http.StatusUnauthorized, msg, args))
	return
}

// PaymentRequired have 3 args (message,data,extra)
func PaymentRequired(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusPaymentRequired, http.StatusOK).(int), loopArgs(ctx, http.StatusPaymentRequired, msg, args))
	return
}

// Forbidden have 3 args (message,data,extra)
func Forbidden(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusForbidden, http.StatusOK).(int), loopArgs(ctx, http.StatusForbidden, msg, args))
	return
}

// NotFound have 3 args (message,data,extra)
func NotFound(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusNotFound, http.StatusOK).(int), loopArgs(ctx, http.StatusNotFound, msg, args))
	return
}

// MethodNotAllowed have 3 args (message,data,extra)
func MethodNotAllowed(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusMethodNotAllowed, http.StatusOK).(int), loopArgs(ctx, http.StatusMethodNotAllowed, msg, args))
	return
}

// NotAcceptable have 3 args (message,data,extra)
func NotAcceptable(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusNotAcceptable, http.StatusOK).(int), loopArgs(ctx, http.StatusNotAcceptable, msg, args))
	return
}

// ProxyAuthRequired have 3 args (message,data,extra)
func ProxyAuthRequired(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusProxyAuthRequired, http.StatusOK).(int), loopArgs(ctx, http.StatusProxyAuthRequired, msg, args))
	return
}

// RequestTimeOut have 3 args (message,data,extra)
func RequestTimeOut(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusRequestTimeout, http.StatusOK).(int), loopArgs(ctx, http.StatusRequestTimeout, msg, args))
	return
}

// Conflict have 3 args (message,data,extra)
func Conflict(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusConflict, http.StatusOK).(int), loopArgs(ctx, http.StatusConflict, msg, args))
	return
}

// Gone have 3 args (message,data,extra)
func Gone(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusGone, http.StatusOK).(int), loopArgs(ctx, http.StatusGone, msg, args))
	return
}

// ToManyRequests have 3 args (message,data,extra)
func ToManyRequests(ctx *gin.Context, msg M, args ...interface{}) {
	ctx.SecureJSON(utils.Eq(statusCode, true, http.StatusTooManyRequests, http.StatusOK).(int), loopArgs(ctx, http.StatusTooManyRequests, msg, args))
	return
}
