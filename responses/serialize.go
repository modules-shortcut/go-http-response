package responses

import (
	"gitlab.com/modules-shortcut/go-utils"
	"reflect"
	"strings"
)

type Serialize struct {
	data interface{}
	keys []string
}

func (s *Serialize) SetData(data interface{}) {
	s.data = data
	return
}

func (s *Serialize) SetKeys(keys ...string) {
	s.keys = keys
	return
}

func NewSerialize(data interface{}, keys ...string) *Serialize {
	return &Serialize{
		data: data,
		keys: keys,
	}
}

func isSlice(v interface{}) bool {
	return v != nil && reflect.TypeOf(v).Kind() == reflect.Slice
}

func (s *Serialize) Exclude() interface{} {
	for _, key := range s.keys {
		s.data = loop(s.data, key)
	}
	return s.data
}

func loop(data interface{}, key string) interface{} {
	//check if data is slice
	if !isSlice(data) {
		var temp map[string]interface{}
		if err := utils.Convert(data, &temp); err != nil {
			utils.ErrorHandler(err)
			return data
		}

		//get key
		keys := strings.Split(key, ".")
		if len(keys) > 2 {
			if temp[keys[0]] != nil {
				temp[keys[0]] = loop(temp[keys[0]], strings.Join(keys[1:], "."))
			}
		} else if len(keys) > 1 {
			temp[keys[0]] = loop(temp[keys[0]], keys[1])
		} else {
			delete(temp, keys[0])
		}
		return temp
	}

	//if data is slice
	var (
		temps []map[string]interface{}
		res   []interface{}
	)
	if err := utils.Convert(data, &temps); err != nil {
		return data
	}

	for _, elm := range temps {
		res = append(res, loop(elm, key))
	}
	return res
}

//TODO: add serialize by include all key only
